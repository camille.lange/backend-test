drop table if exists `Bep_Prescription`;
drop table if exists `Bep_HistoMaladie`;
drop table if exists `Bep_Patient`;
drop table if exists `Bep_Maladie`;
drop table if exists `Bep_Medicament`;

CREATE TABLE `Bep_Patient` 
(
    `NumPat` int AUTO_INCREMENT,
    `Nom` varChar(10),
    `Prenom` varChar(10),
    primary key(`NumPat`)
);

CREATE TABLE `Bep_HistoMaladie`
(
    `NumPat` int,
    `NumMaladie` int,
    `Date` Date,
    primary key(`NumPat`, `NumMaladie`, `Date`)
);

CREATE TABLE `Bep_Maladie`
(
    `NumMaladie` int AUTO_INCREMENT,
    `ma_intitule` varChar(20),
    primary key(`NumMaladie`)
);

CREATE TABLE `Bep_Prescription`
(
    `NumPat` int,
    `NumMedicament` int,
    primary key(`NumPat`, `NumMedicament`)
);

CREATE TABLE `Bep_Medicament`
(
    `NumMedicament` int AUTO_INCREMENT,
    `me_intitule` varChar(20),
    primary key(`NumMedicament`)
);

ALTER TABLE `Bep_HistoMaladie`
  ADD FOREIGN KEY (`NumPat`) REFERENCES `Bep_Patient` (`NumPat`),
  ADD FOREIGN KEY (`NumMaladie`) REFERENCES `Bep_Maladie` (`NumMaladie`);


ALTER TABLE `Bep_Prescription`
  ADD FOREIGN KEY (`NumPat`) REFERENCES `Bep_Patient` (`NumPat`),
  ADD FOREIGN KEY (`NumMedicament`) REFERENCES `Bep_Medicament` (`NumMedicament`);
  
  
INSERT INTO `Bep_Patient`(`Nom`, `Prenom`) VALUES ("Audabram","Luc");  
INSERT INTO `Bep_Patient`(`Nom`, `Prenom`) VALUES ("Victoire","Robillard");  
INSERT INTO `Bep_Patient`(`Nom`, `Prenom`) VALUES ("Guerin","Simard");  

INSERT INTO `Bep_Maladie`(`ma_intitule`) VALUES ("Rhume");  
INSERT INTO `Bep_Maladie`(`ma_intitule`) VALUES ("Gastro");  
INSERT INTO `Bep_Maladie`(`ma_intitule`) VALUES ("Alergie");  

INSERT INTO `Bep_Medicament`(`me_intitule`) VALUES ("Aspirine");  
INSERT INTO `Bep_Medicament`(`me_intitule`) VALUES ("Doliprane");  
INSERT INTO `Bep_Medicament`(`me_intitule`) VALUES ("Antihistaminique");  

INSERT INTO `Bep_HistoMaladie` (`NumPat`, `NumMaladie`, `Date`) VALUES (1,1,sysdate());
INSERT INTO `Bep_HistoMaladie` (`NumPat`, `NumMaladie`, `Date`) VALUES (1,3,sysdate());
INSERT INTO `Bep_HistoMaladie` (`NumPat`, `NumMaladie`, `Date`) VALUES (1,3,STR_TO_DATE('14/05/2020','%d/%m/%Y'));
INSERT INTO `Bep_Prescription` (`NumPat`, `NumMedicament`) VALUES (1,3);

INSERT INTO `Bep_HistoMaladie` (`NumPat`, `NumMaladie`, `Date`) VALUES (2,2,sysdate());
INSERT INTO `Bep_Prescription` (`NumPat`, `NumMedicament`) VALUES (2,2);

INSERT INTO `Bep_HistoMaladie` (`NumPat`, `NumMaladie`, `Date`) VALUES (3,1,STR_TO_DATE('5/9/2019','%d/%m/%Y'));
