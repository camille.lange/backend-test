module ProjetBackend

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
)
