package main

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

var wg sync.WaitGroup

const rootCert = `-----BEGIN CERTIFICATE-----
MIICwzCCAaugAwIBAgIJAJtri+yD+WqIMA0GCSqGSIb3DQEBBQUAMBQxEjAQBgNV
BAMTCWxvY2FsaG9zdDAeFw0yMDA2MTMxMjQ5MzBaFw0zMDA2MTExMjQ5MzBaMBQx
EjAQBgNVBAMTCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAMhk/PLQR8ZhF1w6n/e4IT2NQdYnULyv1XzjjH8xOSHwkzRu/aXgWU8ITkI4
PXqUTKmIZePWagme+FLphHPfoXe0wVhj8d/bdcywEQw0pU+EUx2eSn4RyzNsHuzT
H9P9oC9a1hHZ76+vaatvJUKfYZ288j8MDFgAFxbTONoi9JijRUruSRgX35rFdP0K
GnjxsmVMI5v/IOKLEBvE6Qpfct0YV94OI0Eji3aSKkS6R0wDkodNkWZOQT0tVeXj
eAdvMKR9Z1ECFai83cwhc07WhMkE3yo/hKPgpfeI5d2tl0OCFMMhnH2axABOjPCr
iCjafGfJnVSbiEwKkYeUsoeEAM8CAwEAAaMYMBYwFAYDVR0RBA0wC4IJbG9jYWxo
b3N0MA0GCSqGSIb3DQEBBQUAA4IBAQChCkprDhKdnSIa1ALkYYhyrnuviXxq9gUv
bTtoHMG4ifNJU6c51gOKG98k9vumfEYX9esyt+YuMx2MzCqTvWs7zd6vPzeDW7u0
VaSHMZMbdRbkmm/dTNc8kBbA5He2B/MYazVD86oLk3Uz0rjfiy/xRFybShmzCRzu
wC8u6xBlyvMb2uZlEmwSfbh1+GWlWc/i4S4/XotO+oQ8knPIGu2KmJMoLTBgbCnB
fVYZGTZA2XfkC9KegD17bcth7kcUxKNfuA3uSnNfVha8D2wmR+1uE/PKUDc5BqWn
HT9mayHgdiVa3ugY2Vi/nCHzXllIn4uZAxRoUKDiJ9ETEQXbRDFW
-----END CERTIFICATE-----
`

func main() {
	roots := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(rootCert))
	if !ok {
		log.Fatal("failed to parse root certificate")
	}
	config := &tls.Config{RootCAs: roots, ServerName: "localhost"}

	c, err := tls.Dial("tcp", "localhost:6000", config)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	go lecture(c)
	go envoie(c)

	wg.Add(1)
	wg.Wait()
}

func lecture(c net.Conn) {
	defer wg.Done()

	for {
		message, _ := bufio.NewReader(c).ReadString('*')
		message = strings.TrimSpace(message)
		message = strings.Trim(message,"*")

		if message == "" {
			c.Close()
			break
		}else{
			fmt.Println("-> "+message)
		}
	}
}

func envoie(c net.Conn) {
	reader := bufio.NewReader(os.Stdin)

	for{
		text, _ := reader.ReadString('\n')
		fmt.Fprintf(c, text+"*")
	}
}
